﻿using System;
using System.Web.Services;
using System.Web.UI;

namespace RegistrationPages
{
	public partial class Recovery : Page
	{
		protected void PageLoad(object sender, EventArgs e) {
		}

		[WebMethod]
		public static object SendCodeForPasswordChange(string number, string type) {
			var cookie = Utils.TryLogin();
			return Utils.ExecuteConfigurationService(cookie, "EkoUserManagementService", "SendCodeForPasswordChange", new {
				number,
				type
			});
		}

		[WebMethod]
		public static object VerifyCodeForPasswordChange(Guid codeId, string code) {
			var cookie = Utils.TryLogin();
			return Utils.ExecuteConfigurationService(cookie, "EkoUserManagementService", "VerifyCodeForPasswordChange", new {
				code,
				codeId
			});
		}

		[WebMethod]
		public static object ChangePassword(Guid codeId, string code, string password, string type) {
			var cookie = Utils.TryLogin();
			return Utils.ExecuteConfigurationService(cookie, "EkoUserManagementService", "ChangePassword", new {
				code,
				codeId,
				password,
				type
			});
		}
	}
}